use chrono::{DateTime, Duration, TimeZone, Utc};

#[derive(Clone, Copy, Debug, PartialEq)]
pub struct Timestamp {
    pub second_fractions: u64,
    pub seconds: i64,
}

const FRACTIONS_PER_NS: u64 = 18446744073; // 2 ** 64 / 10 ** 9;

impl Timestamp {
    pub fn new(seconds: i64, second_fractions: u64) -> Timestamp {
        Timestamp {
            seconds,
            second_fractions,
        }
    }

    pub fn to_datetime(&self) -> Option<DateTime<Utc>> {
        let seconds_duration = Duration::seconds(self.seconds);
        let fractions_duration =
            Duration::nanoseconds((self.second_fractions / FRACTIONS_PER_NS) as i64);
        let epoch = Utc.ymd(1904, 1, 1).and_hms(0, 0, 0);
        epoch
            .checked_add_signed(seconds_duration)
            .and_then(|dt| dt.checked_add_signed(fractions_duration))
    }

    pub fn from_datetime(dt: DateTime<Utc>) -> Timestamp {
        let epoch = Utc.ymd(1904, 1, 1).and_hms(0, 0, 0);
        let seconds_duration = dt.signed_duration_since(epoch);
        let seconds = seconds_duration.num_seconds();
        let fractions_duration = seconds_duration - Duration::seconds(seconds);
        let second_fractions =
            fractions_duration.num_nanoseconds().unwrap() as u64 * FRACTIONS_PER_NS;
        Timestamp {
            seconds,
            second_fractions,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use chrono::{TimeZone, Timelike, Utc};

    #[test]
    fn test_convert_datetime() {
        let timestamp = Timestamp::new(0, 0);
        let dt = timestamp.to_datetime().unwrap();
        assert_eq!(dt, Utc.ymd(1904, 1, 1).and_hms(0, 0, 0));
        assert_eq!(timestamp, Timestamp::from_datetime(dt));

        let timestamp = Timestamp::new(1, 0);
        let dt = timestamp.to_datetime().unwrap();
        assert_eq!(dt, Utc.ymd(1904, 1, 1).and_hms(0, 0, 1));
        assert_eq!(timestamp, Timestamp::from_datetime(dt));

        let timestamp = Timestamp::from_datetime(
            Utc.ymd(1904, 1, 1)
                .and_hms(0, 0, 1)
                .with_nanosecond(5)
                .unwrap(),
        );
        let dt = timestamp.to_datetime().unwrap();
        assert_eq!(
            dt,
            Utc.ymd(1904, 1, 1)
                .and_hms(0, 0, 1)
                .with_nanosecond(5)
                .unwrap()
        );
        assert_eq!(timestamp, Timestamp::from_datetime(dt));

        let timestamp = Timestamp::from_datetime(Utc.ymd(1904, 1, 1).and_hms(0, 0, 2));
        let dt = timestamp.to_datetime().unwrap();
        assert_eq!(dt, Utc.ymd(1904, 1, 1).and_hms(0, 0, 2));
        assert_eq!(timestamp, Timestamp::from_datetime(dt));

        let timestamp = Timestamp::from_datetime(
            Utc.ymd(1904, 1, 1)
                .and_hms(0, 0, 2)
                .with_nanosecond(5)
                .unwrap(),
        );
        let dt = timestamp.to_datetime().unwrap();
        assert_eq!(
            dt,
            Utc.ymd(1904, 1, 1)
                .and_hms(0, 0, 2)
                .with_nanosecond(5)
                .unwrap()
        );
        assert_eq!(timestamp, Timestamp::from_datetime(dt));
    }
}
